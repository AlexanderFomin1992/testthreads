package com.example.afomin.testthreads;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.reactivestreams.Subscriber;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "multithreading";
    Observable right;
    Observable left;
    Disposable disposable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.err.println("start test");
        OneLeg leftLeg = new OneLeg("left");
        OneLeg rightLeg = new OneLeg("right");
//        Observer<String> observer = new Observer<String>() {
//
//            @Override
//            public void onSubscribe(Disposable d) {
//                System.err.println("observer onSubscribe");
//            }
//
//            @Override
//            public void onNext(String s) {
//                System.err.println(s);
//                ((TextView)findViewById(R.id.textViewLeg)).setText(s);
//            }
//
//            @Override
//            public void onError(Throwable e) {
//                System.err.println("observer onError");
//            }
//
//            @Override
//            public void onComplete() {
//                System.err.println("observer complete");
//            }
//        };
        left = Observable.fromCallable(leftLeg);
        right = Observable.fromCallable(rightLeg);
        disposable = right.mergeWith(left).
                subscribeOn(Schedulers.computation()).
                observeOn(AndroidSchedulers.mainThread()).
                repeat().
                subscribe(new Consumer<String>() {
                              @Override
                              public void accept(String s) throws Exception {
                                  // onNext
                                  System.err.println(s);
                                  ((TextView)findViewById(R.id.textViewLeg)).setText(s);
                              }
                          },
                        new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {
                                // onError
                                System.err.println("observer onError");
                            }
                        },
                        new Action() {
                            @Override
                            public void run() throws Exception {
                                // onComplete
                                System.err.println("observer complete");
                            }
                        },
                        new Consumer<Disposable>() {
                            @Override
                            public void accept(Disposable disposable) throws Exception {
                                // onSubscribe
                                System.err.println("observer onSubscribe");
                            }
                        });
    }

    private static class OneLeg implements Callable<String> {
        String legName;
        public OneLeg(String legName) {
            this.legName = legName;
        }

        @Override
        public String call() throws Exception {
            Thread.sleep(1000);
            return this.legName;
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        System.err.println(disposable.isDisposed());
        disposable.dispose();
        System.err.println(disposable.isDisposed());
//        left.unsubscribeOn(Schedulers.computation());
//        right.unsubscribeOn(Schedulers.computation());
    }
}
